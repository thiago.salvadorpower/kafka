package br.com.valecard.producermessage.dto;

import br.com.valecard.producermessage.model.Evento;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageKafka {

    private Evento evento;
    private String nomeTopic;
    private String nomeCliente;
    private String tipoEvento;
    private String dataEnvio;
}
