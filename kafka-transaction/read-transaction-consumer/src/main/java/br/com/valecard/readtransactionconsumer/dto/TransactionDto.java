package br.com.valecard.readtransactionconsumer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDto {
    private String id_created;
    private String fund_account_id;
    private Boolean processed;
    private BigDecimal amount;
    private String message;
    private String fees;
    private String status;
    private String purpose;
    private String utr;
    private String mode;
    private String reference_id;
    private String narration;
    private String batch_id;
    private String failure_reason;
    private LocalDateTime created_at;
    private String fee_type;
}
