package br.com.valecard.producermessage.repository;

import br.com.valecard.producermessage.model.ClientByEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientByEventRepository extends MongoRepository<ClientByEvent, String> {
    List<ClientByEvent> findClienteByCnpjCliente(String cnpjCliente);

}
