package br.com.valecard.producertransaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerTransactionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerTransactionApplication.class, args);
	}

}
