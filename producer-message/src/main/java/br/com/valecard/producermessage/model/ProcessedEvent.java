package br.com.valecard.producermessage.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "processed_event")
public class ProcessedEvent {

    @Id
    private String id;
    private Evento evento;
    private Topico topic;
    private String nomeCliente;
    private String tipoEvento;
    private String dataEnvio;
    private String dataLeitura;
}