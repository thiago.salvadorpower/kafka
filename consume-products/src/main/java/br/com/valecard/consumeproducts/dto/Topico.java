package br.com.valecard.consumeproducts.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Topico {
    private String nomeTopico;
    private Integer partition;
    private Long offset;
}
