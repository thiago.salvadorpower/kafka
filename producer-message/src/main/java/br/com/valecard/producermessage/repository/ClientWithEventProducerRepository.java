package br.com.valecard.producermessage.repository;

import br.com.valecard.producermessage.model.ClientWithEventProducer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientWithEventProducerRepository extends MongoRepository<ClientWithEventProducer, String> {
    @Query("{}")
    Page<ClientWithEventProducer> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<ClientWithEventProducer> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<ClientWithEventProducer> findOneWithEagerRelationships(String id);

}
