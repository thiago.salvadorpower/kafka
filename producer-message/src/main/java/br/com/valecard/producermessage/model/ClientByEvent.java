package br.com.valecard.producermessage.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Document(collection = "client_by_event")
public class ClientByEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("nome")
    private String nome;

    @NotNull
    @Field("cnpj_cliente")
    private String cnpjCliente;

    @NotNull
    @Field("nome_topic")
    private String nomeTopic;

    @NotNull
    @Field("tipo_topico")
    private Integer tipoTopico;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getId() {
        return this.id;
    }

    public ClientByEvent id(String id) {
        this.setId(id);
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public ClientByEvent nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpjCliente() {
        return this.cnpjCliente;
    }

    public ClientByEvent cnpjCliente(String cnpjCliente) {
        this.setCnpjCliente(cnpjCliente);
        return this;
    }

    public void setCnpjCliente(String cnpjCliente) {
        this.cnpjCliente = cnpjCliente;
    }

    public String getNomeTopic() {
        return this.nomeTopic;
    }

    public ClientByEvent nomeTopic(String nomeTopic) {
        this.setNomeTopic(nomeTopic);
        return this;
    }

    public void setNomeTopic(String nomeTopic) {
        this.nomeTopic = nomeTopic;
    }

    public Integer getTipoTopico() {
        return this.tipoTopico;
    }

    public ClientByEvent tipoTopico(Integer tipoTopico) {
        this.setTipoTopico(tipoTopico);
        return this;
    }

    public void setTipoTopico(Integer tipoTopico) {
        this.tipoTopico = tipoTopico;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientByEvent)) {
            return false;
        }
        return id != null && id.equals(((ClientByEvent) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientByEvent{" +
                "id=" + getId() +
                ", nome='" + getNome() + "'" +
                ", cnpjCliente='" + getCnpjCliente() + "'" +
                ", nomeTopic='" + getNomeTopic() + "'" +
                ", tipoTopico=" + getTipoTopico() +
                "}";
    }
}
