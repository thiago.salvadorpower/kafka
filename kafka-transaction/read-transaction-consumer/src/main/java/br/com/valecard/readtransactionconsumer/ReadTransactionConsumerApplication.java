package br.com.valecard.readtransactionconsumer;

import br.com.valecard.readtransactionconsumer.useCases.ProcessTransactionUseCase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;

@SpringBootApplication()
public class ReadTransactionConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadTransactionConsumerApplication.class, args);
	}

}
