package br.com.valecard.transactionproducer.config;

import br.com.valecard.transactionproducer.controller.TransactionDto;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaProducerConfig {

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Value(value = "${topic.name}")
    private String topic;

//    @Bean
//    public NewTopic createTopic(){
//        return new NewTopic(topic,3,(short)1);
//    }
    private static final String TRANSACTION_ID = "transaction-001";

    @Bean
    public ProducerFactory<String, TransactionDto> TransactionProducerFactory(){
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, Config.BROKER_URL);
        configProps.put(ProducerConfig.CLIENT_ID_CONFIG, Config.INPUT_TOPIC);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        configProps.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, TRANSACTION_ID);
        configProps.put(ProducerConfig.ACKS_CONFIG, "-1");
        configProps.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");

        configProps.put("ssl.endpoint.identification.algorithm", "https");
        configProps.put("security.protocol", "SASL_SSL");
        configProps.put("sasl.mechanism", "PLAIN");
        configProps.put("sasl.jaas.config", String.format("org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%s\" password=\"%s\";", Config.APIKEY, Config.SECRET));
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    public KafkaTemplate<String, TransactionDto> transactionKafkaTemplate() {
        return new KafkaTemplate<>(TransactionProducerFactory());
    }
}
