package br.com.valecard.producermessage.controller;

import br.com.valecard.producermessage.dto.EventWebHookPayload;
import br.com.valecard.producermessage.repository.ProcessedEventRepository;
import br.com.valecard.producermessage.service.EventWebHookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/webhook/event")
public class EventoWebHookController {

    @Autowired
    ProcessedEventRepository repository;

    @Autowired
    EventWebHookService service;

    @PostMapping
    public ResponseEntity<Object> create(@RequestBody EventWebHookPayload payload) {
        //LocalDateTime.now()
        service.sendMessage(payload);
        return ResponseEntity.status(HttpStatus.CREATED).body( "ok");
    }



    @GetMapping
    public ResponseEntity<Object> getEventSaved() {
        //LocalDateTime.now()
        return ResponseEntity.status(HttpStatus.CREATED).body( repository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getByID(@PathVariable String id) {
        //LocalDateTime.now()
        return ResponseEntity.status(HttpStatus.CREATED).body( repository.findById(id));
    }
}
