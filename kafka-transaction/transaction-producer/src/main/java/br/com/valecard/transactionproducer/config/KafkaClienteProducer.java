//package br.com.valecard.transactionproducer.config;
//
//import br.com.valecard.transactionproducer.controller.TransactionDto;
//import com.fasterxml.jackson.databind.JsonSerializer;
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.common.serialization.StringSerializer;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.ExecutionException;
//
//public class KafkaClienteProducer {
//
//    private static final String TRANSACTION_ID = "transaction-001";
//
//    public KafkaProducer<String, TransactionDto> TransactionProducerFactory() {
//        Map<String, Object> configProps = new HashMap<>();
//        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
//        configProps.put(ProducerConfig.CLIENT_ID_CONFIG, "INPUT-TOPIC");
//        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
//
//        configProps.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, TRANSACTION_ID);
//        configProps.put(ProducerConfig.ACKS_CONFIG, "all");
//        configProps.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
//
//        /*configProps.put("ssl.endpoint.identification.algorithm", "https");
//        configProps.put("security.protocol", "SASL_SSL");
//        configProps.put("sasl.mechanism", "PLAIN");*/
//        return new KafkaProducer<>(configProps);
//    }
//}
