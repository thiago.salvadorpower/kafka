package br.com.valecard.confirmedtransactionconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfirmedTransactionConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfirmedTransactionConsumerApplication.class, args);
	}

}
