package br.com.valecard.readtransactionconsumer.config;

import br.com.valecard.readtransactionconsumer.dto.TransactionDto;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;


public class kafkaProducerConfig {

    private static final String TRANSACTION_ID = "transaction-001";

    public  KafkaProducer<String, TransactionDto> TransactionProducerFactory(){
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, Config.BROKER_URL);
        configProps.put(ProducerConfig.CLIENT_ID_CONFIG, Config.OUTPUT_TOPIC);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        configProps.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, TRANSACTION_ID);
        configProps.put(ProducerConfig.ACKS_CONFIG, "all");
        configProps.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");

        configProps.put("ssl.endpoint.identification.algorithm", "https");
        configProps.put("security.protocol", "SASL_SSL");
        configProps.put("sasl.mechanism", "PLAIN");
        configProps.put("sasl.jaas.config", String.format("org.apache.kafka.common.security.plain.PlainLoginModule required username=\"%s\" password=\"%s\";", Config.APIKEY, Config.SECRET));
        return new KafkaProducer<>(configProps);

    }

}
