package br.com.valecard.producermessage.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Document(collection = "client_with_event_producer")
public class ClientWithEventProducer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @DBRef
    @Field("cliente")
    private Cliente cliente;

    @DBRef
    @Field("topicoEvent")
    private TopicoEvent topicoEvent;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getId() {
        return this.id;
    }

    public ClientWithEventProducer id(String id) {
        this.setId(id);
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClientWithEventProducer cliente(Cliente cliente) {
        this.setCliente(cliente);
        return this;
    }

    public TopicoEvent getTopicoEvent() {
        return this.topicoEvent;
    }

    public void setTopicoEvent(TopicoEvent topicoEvent) {
        this.topicoEvent = topicoEvent;
    }

    public ClientWithEventProducer topicoEvent(TopicoEvent topicoEvent) {
        this.setTopicoEvent(topicoEvent);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientWithEventProducer)) {
            return false;
        }
        return id != null && id.equals(((ClientWithEventProducer) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientWithEventProducer{" +
                "id=" + getId() +
                "}";
    }
}
