package br.com.valecard.producertransaction.producer;

import br.com.valecard.producertransaction.config.KafkaClienteProducer;
import br.com.valecard.producertransaction.dto.TransactionDto;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.stereotype.Service;

@Service
public class TransactionProducer {
    public void sendMessage(TransactionDto transactionDto) {
        KafkaClienteProducer producer = new KafkaClienteProducer();

        var kafkaProducer = producer.TransactionProducerFactory();

        if(transactionDto != null){
            kafkaProducer.send(new ProducerRecord<>("transaction-input-topic",transactionDto));
            kafkaProducer.flush();
        }

    }
}
