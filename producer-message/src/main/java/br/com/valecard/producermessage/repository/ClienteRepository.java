package br.com.valecard.producermessage.repository;

import br.com.valecard.producermessage.model.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends MongoRepository<Cliente, String> {

    Cliente findOneClienteByCnpjCliente(String cnpjCliente);
}
