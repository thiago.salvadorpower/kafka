package br.com.valecard.producermessage.dto;


import br.com.valecard.producermessage.model.Evento;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EventWebHookPayload {
    private String cnpjCliente;
    private Integer identificadorEvento;
    private Evento evento;
}
