package br.com.valecard.producermessage.repository;

import br.com.valecard.producermessage.model.TopicoEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicoEventRepository extends MongoRepository<TopicoEvent, String> {}

