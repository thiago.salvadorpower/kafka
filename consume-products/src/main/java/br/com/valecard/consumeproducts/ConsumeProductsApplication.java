package br.com.valecard.consumeproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumeProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumeProductsApplication.class, args);
	}

}
