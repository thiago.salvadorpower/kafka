package br.com.valecard.producertransaction.Controller;

import br.com.valecard.producertransaction.dto.TransactionDto;
import br.com.valecard.producertransaction.producer.TransactionProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

@RestController
@RequestMapping("/transaction")
public class TransactionPostController {

    TransactionProducer producer;

    public TransactionPostController(TransactionProducer producer) {
        this.producer = producer;
    }


    @PostMapping
    public ResponseEntity<TransactionDto> create(@RequestBody TransactionDto payloadTransaction) {

        TransactionDto transaction = TransactionDto.builder()
                .id_created(UUID.randomUUID().toString())
                .fund_account_id(payloadTransaction.getFund_account_id())
                .processed(payloadTransaction.getProcessed())
                .amount(payloadTransaction.getAmount())
                .message(payloadTransaction.getMessage())
                .fees(payloadTransaction.getFees())
                .status(payloadTransaction.getStatus())
                .purpose(payloadTransaction.getPurpose())
                .utr(payloadTransaction.getUtr())
                .mode(payloadTransaction.getMode())
                .reference_id(payloadTransaction.getReference_id())
                .narration(payloadTransaction.getNarration())
                .batch_id(payloadTransaction.getBatch_id())
                .failure_reason(payloadTransaction.getFailure_reason())
                .created_at(LocalDateTime.now())
                .fee_type(payloadTransaction.getFee_type())
                .build();

        producer.sendMessage(transaction);
        return ResponseEntity.status(HttpStatus.CREATED).body(transaction);
    }
}
