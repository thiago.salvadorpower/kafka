package br.com.valecard.readtransactionconsumer.config;

public final class Config {
    public static final String APIKEY = "xxx";
    public static final String SECRET = "xxx";
    public static final String BROKER_URL = "xxx";
    public static final String INPUT_TOPIC = "transaction-input-topic";
    public static final String OUTPUT_TOPIC = "transaction-outPut-topic";
}
