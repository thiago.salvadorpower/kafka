package br.com.valecard.consumeproducts.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "event-sent")
@NoArgsConstructor
@AllArgsConstructor
public class ProcessedEvent {
    @Id
    private String id;
    private String nomeEvento;
    private String descricaoEvento;
    private String nomeTopicoEnviado;
    private String inforTopicoNome;
    private Integer inforTopicoPartition;
    private Long inforTopicoOffset;
    private String nomeCliente;
    private String tipoEventoRecebido;
    private String dataEnvioEvento;
    private String dataLeituraEvento;
}
