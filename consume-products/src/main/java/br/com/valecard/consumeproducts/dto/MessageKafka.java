package br.com.valecard.consumeproducts.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageKafka {

    private Evento evento;
    private String nomeTopic;
    private String nomeCliente;
    private String tipoEvento;
    private String dataEnvio;
}
