package br.com.valecard.producermessage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebhookProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebhookProducerApplication.class, args);
	}

}
