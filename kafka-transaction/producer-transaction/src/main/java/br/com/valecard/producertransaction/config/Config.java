package br.com.valecard.producertransaction.config;

public final class Config {
    public static final String APIKEY = "xxx";
    public static final String SECRET = "xxx";
    public static final String BROKER_URL = "xxx";
    public static final String INPUT_TOPIC = "transaction-input-topic";
}
