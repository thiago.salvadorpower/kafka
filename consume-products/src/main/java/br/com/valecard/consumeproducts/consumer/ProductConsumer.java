package br.com.valecard.consumeproducts.consumer;

import br.com.valecard.consumeproducts.dto.MessageKafka;
import br.com.valecard.consumeproducts.dto.ProcessedEvent;
import br.com.valecard.consumeproducts.dto.Topico;
import br.com.valecard.consumeproducts.repository.ProcessedEventRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class ProductConsumer {
    private static final Logger log = LoggerFactory.getLogger(ProductConsumer.class);

    @Autowired
    ProcessedEventRepository eventRepository;

    @Value(value = "${spring.kafka.group-id}")
    private String groupId;

    @KafkaListener(topicPattern = ".*", groupId = "${spring.kafka.group-id}", containerFactory = "productConcurrentKafkaListenerContainerFactory")
    public void listenTopicProduct(ConsumerRecord<String, MessageKafka> record) {

            var processedEvent =eventRepository.save(
                    ProcessedEvent.builder()
                            .nomeEvento(record.value().getEvento().getNomeEvento())
                            .descricaoEvento(record.value().getEvento().getDescricao())
                            .nomeTopicoEnviado(record.value().getNomeTopic())
                            .inforTopicoNome(record.topic())
                            .inforTopicoPartition(record.partition())
                            .inforTopicoOffset(record.offset())
                            .nomeCliente(record.value().getNomeCliente())
                            .tipoEventoRecebido(record.value().getTipoEvento())
                            .dataEnvioEvento(record.value().getDataEnvio())
                            .dataLeituraEvento(LocalDate.now().toString())
                            .build());

        log.info("Saved {}", processedEvent.getId() );
        log.info("Received Message " + record.value().toString() + "Received Message " + record.topic());

    }

    }




