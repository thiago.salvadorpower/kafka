package br.com.valecard.producermessage.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Document(collection = "topico_event")
public class TopicoEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("nome_topic")
    private String nomeTopic;

    @Field("tipo_topico")
    private Integer tipoTopico;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getId() {
        return this.id;
    }

    public TopicoEvent id(String id) {
        this.setId(id);
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNomeTopic() {
        return this.nomeTopic;
    }

    public TopicoEvent nomeTopic(String nomeTopic) {
        this.setNomeTopic(nomeTopic);
        return this;
    }

    public void setNomeTopic(String nomeTopic) {
        this.nomeTopic = nomeTopic;
    }

    public Integer getTipoTopico() {
        return this.tipoTopico;
    }

    public TopicoEvent tipoTopico(Integer tipoTopico) {
        this.setTipoTopico(tipoTopico);
        return this;
    }

    public void setTipoTopico(Integer tipoTopico) {
        this.tipoTopico = tipoTopico;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TopicoEvent)) {
            return false;
        }
        return id != null && id.equals(((TopicoEvent) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TopicoEvent{" +
                "id=" + getId() +
                ", nomeTopic='" + getNomeTopic() + "'" +
                ", tipoTopico=" + getTipoTopico() +
                "}";
    }
}
