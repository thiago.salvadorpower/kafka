package br.com.valecard.readtransactionconsumer.useCases;

import br.com.valecard.readtransactionconsumer.config.Config;
import br.com.valecard.readtransactionconsumer.config.kafkaConsumerConfig;
import br.com.valecard.readtransactionconsumer.config.kafkaProducerConfig;
import br.com.valecard.readtransactionconsumer.dto.TransactionDto;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;


@Component
public class ProcessTransactionUseCase {

    private static final String CONSUMER_GROUP_ID = "consumer-group-transact-05";

    @PostConstruct
    public void transction() throws ExecutionException, InterruptedException{
        //Cria o produtor
        kafkaProducerConfig producer = new kafkaProducerConfig();
        KafkaProducer<String, TransactionDto> kafkaProducer = producer.TransactionProducerFactory();

        //Inicializa as configurações deste produtor no Coordinator do Cluster
        kafkaProducer.initTransactions();

        //Cria o Consumidor e inscreve o mesmo no INPUT-TOPIC
        kafkaConsumerConfig consumer = new kafkaConsumerConfig();
        KafkaConsumer<String, TransactionDto> kafkaConsumer = consumer.transactionConsumerFactory();
        kafkaConsumer.subscribe(Collections.singleton(Config.INPUT_TOPIC));

        var recordCount = 0;

        do{
            //Map responsável por guardar quais offsets devem ser comitados no fim da transação ('All or Nothing')
            Map<TopicPartition, OffsetAndMetadata> offsets = new HashMap<>();

            //Aguarda por no máximo 1minuto por mensagens a serem consumidas
            ConsumerRecords<String, TransactionDto> records = kafkaConsumer.poll(Duration.ofSeconds(10));
            if(records.isEmpty()){
                kafkaProducer.close();
                break;
            }
            recordCount = records.count();



            //Ao chegar neste ponto significa que mensagens estão sendo consumidas, então podemos iniciar uma transação
            kafkaProducer.beginTransaction();
            AtomicBoolean aborted = new AtomicBoolean(false);

            records.forEach( record -> {
                //Para cada mensagem consumida nós realizamos uma 'transformação simples', sem comitar o offset de consumo
                if(!(record.value()==null)) {
                    //processed
                    record.value().setMessage("999");
                    record.value().setProcessed(true);
                    kafkaProducer.send(new ProducerRecord<>(Config.OUTPUT_TOPIC, record.value()));

                    if(record.value().getMessage().contains("999") || !record.value().getProcessed()){
                        kafkaProducer.abortTransaction();
                        aborted.set(true);
                        //break;
                    }

                    //Sempre que uma mensagem for 'transformada' com sucesso, gravamos o offset dela no nosso Map, para futuro commit da transação
                    offsets.put(new TopicPartition(Config.INPUT_TOPIC, record.partition()), new OffsetAndMetadata(record.offset() + 1));

                }

            });

            if(!aborted.get()){
                kafkaProducer.sendOffsetsToTransaction(offsets, CONSUMER_GROUP_ID);
                kafkaProducer.commitTransaction();
            }


        }while (recordCount >0);

    }


}











