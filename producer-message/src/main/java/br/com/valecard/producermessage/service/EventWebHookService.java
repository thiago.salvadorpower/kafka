package br.com.valecard.producermessage.service;

import br.com.valecard.producermessage.dto.EventWebHookPayload;
import br.com.valecard.producermessage.dto.MessageKafka;
import br.com.valecard.producermessage.repository.ClientByEventRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class EventWebHookService {

    private final KafkaTemplate<String, MessageKafka> kafkaTemplate;

    @Autowired
    ClientByEventRepository clienteRepository;

    public void sendMessage(EventWebHookPayload eventWebHookPayload) {

        var inforToSend = this.preparaEnvio(eventWebHookPayload);

        kafkaTemplate.send(inforToSend.getNomeTopic(), inforToSend).addCallback(
                success -> log.info("Messagem send" + Objects.requireNonNull(success)),
                failure -> log.info("Message failure" + failure.getMessage())
        );

        kafkaTemplate.flush();
    }

    public MessageKafka preparaEnvio(EventWebHookPayload payload) {

        var listTopicsPerClient = clienteRepository.findClienteByCnpjCliente(payload.getCnpjCliente());

        return MessageKafka.builder()
                .nomeCliente(listTopicsPerClient.stream().findFirst().get().getNome())
                .evento(payload.getEvento())
                .nomeTopic(listTopicsPerClient.stream()
                        .filter(e -> e.getTipoTopico().equals(payload.getIdentificadorEvento()))
                        .findFirst()
                        .get()
                        .getNomeTopic())
                .tipoEvento(payload.getIdentificadorEvento().toString())
                .dataEnvio(LocalDate.now().toString())
                .build();
    }
}
