package br.com.valecard.transactionproducer.producer;

import br.com.valecard.transactionproducer.controller.TransactionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class TransactionProducer {



    private static final Logger logger = LoggerFactory.getLogger(TransactionProducer.class);
    private final String topic;
    private final KafkaTemplate<String, TransactionDto> kafkaTemplate;

    public TransactionProducer(@Value("${topic.name}") String topic, KafkaTemplate<String, TransactionDto> kafkaTemplate){
        this.topic= topic;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessage(TransactionDto transactionDto) {

        kafkaTemplate.send(topic, transactionDto).addCallback(
                success -> logger.info("Message send" + Objects.requireNonNull(success)),
                failure -> logger.info("Message failure" + failure.getMessage())
        );
        kafkaTemplate.flush();
    }

//    public void sendMessage(TransactionDto transactionDto) {
//        KafkaClienteProducer producer = new KafkaClienteProducer();
//
//        var kafkaProducer = producer.TransactionProducerFactory();
//
//        if(transactionDto != null){
//            kafkaProducer.send(new ProducerRecord<>("INPUT-TOPIC",transactionDto));
//            kafkaProducer.flush();
//        }
//
//    }

}
